package org.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Item {

    @JsonProperty("x")
    @JsonDeserialize(using = ItemFieldXDeserializer.class)
    private List<ObjMixte> x;

}
