package org.example;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TypeXDeserializer extends StdDeserializer<List<ObjMixte>> {

    protected TypeXDeserializer() {
        super(TypeFactory.defaultInstance().constructCollectionType(List.class, ObjMixte.class));
    }

    @Override
    public List<ObjMixte> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        JsonNode node = p.getCodec().readTree(p);
        System.out.println("C:" + node.getClass().getSimpleName());
        System.out.println("T:" + node.getNodeType());

        if (node instanceof TextNode) {
            System.out.println("-------------------");
            return Collections.emptyList();
        }
        if (node instanceof IntNode) {
            System.out.println("-------------------");
            return null;
        }
        if (node instanceof BooleanNode) {
            System.out.println("-------------------");
            return null;
        }
        if (node instanceof ArrayNode) {
            int arraySize = node.size();
            System.out.println("Array size : " + arraySize);
            System.out.println("-------------------");
            return new ArrayList<>(arraySize);
        }

//        int id = (Integer) ((IntNode) node.get("id")).numberValue();
        JsonNode nodeX = node.get("x");
        System.out.println(nodeX);

        System.out.println("-------------------");

        return null;
    }

}
