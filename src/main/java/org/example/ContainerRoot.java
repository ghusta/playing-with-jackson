package org.example;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.List;

public class ContainerRoot {

//        @JsonDeserialize(contentUsing = XDeserializer.class)
    @JsonDeserialize(using = TypeXDeserializer.class)
    private List<ObjMixte> x;

    public List<ObjMixte> getX() {
        return x;
    }

    public void setX(List<ObjMixte> x) {
        this.x = x;
    }
}
