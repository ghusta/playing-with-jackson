package org.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class ObjMixte {

    @JsonProperty
    private String a;

    @JsonProperty
    private String b;

    @JsonProperty
    private Number c;

    @JsonProperty
    private LocalDate d;

}
