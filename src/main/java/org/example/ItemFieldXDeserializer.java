package org.example;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class ItemFieldXDeserializer extends StdDeserializer<List<ObjMixte>> {

    protected ItemFieldXDeserializer() {
        super(TypeFactory.defaultInstance().constructCollectionType(List.class, ObjMixte.class));
    }

    @Override
    public List<ObjMixte> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        JsonNode rootNode = p.getCodec().readTree(p);
        boolean isValueNode = rootNode.isValueNode();
        JsonNodeType nodeType = rootNode.getNodeType();
        log.info("JsonNodeType : {}", nodeType);

        if (rootNode instanceof ArrayNode) {
            int arraySize = rootNode.size();
            log.info("Array size : {}", arraySize);
            String jsonContent = rootNode.toString();
            log.info("-------------------");
            List<ObjMixte> list = new ArrayList<>(arraySize);

            for (JsonNode node : rootNode) {
                // deser naturelle
                ObjMixte objMixte = ctxt.readTreeAsValue(node, ObjMixte.class);
                log.debug("objMixte : {}", objMixte);

//                JsonNode nodeA = node.get("a");
//                if (nodeA.isValueNode()) {
//                    list.add(new ObjMixte(nodeA.asText()));
//                }
                list.add(objMixte);
            }

            rootNode.iterator();
            return list;
        }

        if (rootNode instanceof TextNode) {
            return Collections.emptyList();
        }

        throw new JsonParseException("type non traité : " + nodeType.name());
    }

}
