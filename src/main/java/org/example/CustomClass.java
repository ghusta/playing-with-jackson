package org.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CustomClass {

    @JsonProperty
    private Integer a;

    @JsonProperty
    private String b;

    @JsonProperty
    private List<String> c;

}
