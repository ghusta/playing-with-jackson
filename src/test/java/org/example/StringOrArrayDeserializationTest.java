package org.example;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StringOrArrayDeserializationTest {

    @Test
    void deserStringElement() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

//        JavaType customClassCollection = mapper.getTypeFactory().constructCollectionType(List.class, Item.class);
        Item result = mapper.readValue("""
                {
                    "x" : ""
                }
                """, Item.class);

        assertThat(result.getX())
                .isNotNull()
                .isEmpty();
    }

    @Test
    void deserNumberElement() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

        assertThrows(JacksonException.class, () -> {
            Item result = mapper.readValue("""
                    {
                        "x" : 123
                    }
                    """, Item.class);

        });
    }

    @Test
    void deserArrayElement() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

//        JavaType customClassCollection = mapper.getTypeFactory().constructCollectionType(List.class, Item.class);
        Item result = mapper.readValue("""
                {
                    "x" : [
                        {"a" : 1},
                        {"a" : 2, "b" : "Hello"},
                        {"a" : "3", "b" : "", "c" : 152.2, "d" : null}
                    ]
                }
                """, Item.class);

        assertThat(result.getX())
                .isNotEmpty()
                .hasSize(3);
        assertThat(result.getX().get(0))
                .hasFieldOrPropertyWithValue("a", "1")
                .hasFieldOrPropertyWithValue("b", null);
        assertThat(result.getX().get(1))
                .hasFieldOrPropertyWithValue("a", "2")
                .hasFieldOrPropertyWithValue("b", "Hello");
        assertThat(result.getX().get(2))
                .hasFieldOrPropertyWithValue("a", "3")
                .hasFieldOrPropertyWithValue("c", 152.2);
    }

    @Test
    void deserArrayElement_empty() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

//        JavaType customClassCollection = mapper.getTypeFactory().constructCollectionType(List.class, Item.class);
        Item result = mapper.readValue("""
                {
                    "x" : []
                }
                """, Item.class);

        assertThat(result.getX())
                .isNotNull()
                .isEmpty();
    }
}
