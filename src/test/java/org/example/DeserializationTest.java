package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled
class DeserializationTest {

    @Test
    void deser() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

        CustomClass obj1 = mapper.readValue("""
                {
                    "a" : 1,
                    "b" : "hello",
                    "c" : null
                }
                """, CustomClass.class);

        assertThat(obj1).isNotNull();
    }

    @Test
    void deser_mixte() throws JsonProcessingException {
        ObjectMapper mapper = JsonMapper.builder()
                .build();

        JavaType customClassCollection = mapper.getTypeFactory().constructCollectionType(List.class, ContainerRoot.class);
        List<ContainerRoot> listContainerRoot = mapper.readValue("""
                [
                    {
                        "x" : ""
                    },
                    {
                        "x" : 12
                    },
                    {
                        "x" : true
                    },
                    {
                        "x" : [
                            {"a" : 1},
                            {"a" : 2},
                            {"a" : 3}
                        ]
                    },
                    {
                        "x" : []
                    }
                ]
                """, customClassCollection);

        assertThat(listContainerRoot)
                .isNotEmpty()
                .hasSize(5);
    }

}
